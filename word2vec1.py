# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

用gensim学习word2vec https://www.cnblogs.com/pinard/p/7278324.html
"""

# %%
# -*- coding: utf-8 -*-

import jieba
import jieba.analyse

with open('./allwancibiao.txt','r', encoding = 'utf-8') as f:
    document = f.read()
    
    #document_decode = document.decode('GBK')
    
    document_cut = jieba.cut(document)
    #print  ' '.join(jieba_cut)  //如果打印结果，则分词效果消失，后面的result无法显示
    result = ' '.join(document_cut)
    #result = result.encode('utf-8')
    with open('./allwancibiao_segment.txt', 'w', encoding = 'utf-8') as f2:
        f2.write(result)
f.close()
f2.close()

# %%
# import modules & set up logging
import logging
import os
from gensim.models import word2vec

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

sentences = word2vec.LineSentence('./allwancibiao_segment.txt') 

model = word2vec.Word2Vec(sentences, hs=1,min_count=1,window=3,size=100)  

model.save("word2vec.model")
# %%
req_count = 5
for key in model.wv.similar_by_word('数码', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------1-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('显示器', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------2-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('风衣', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------3-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('补肾', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------4-----------")
# %%
print(model.wv.similarity('补气补血', '不宜安神'))
print("---------5-----------")
print(model.wv.similarity('防尘塞', '自拍杆'))
print("---------6-----------")
# %%
print(model.wv.doesnt_match(u"防尘塞 自拍杆 车载支架 皮衣".split()))
print("---------7-----------")
# %%
 