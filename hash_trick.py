# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

文本挖掘预处理之向量化与Hash Trick https://www.cnblogs.com/pinard/p/6688348.html
"""

# %%
from sklearn.feature_extraction.text import CountVectorizer  
vectorizer=CountVectorizer()
corpus=["I come to China to travel", 
    "This is a car polupar in China",          
    "I love tea and Apple ",   
    "The work is to write some papers in science"] 
print (vectorizer.fit_transform(corpus))

# %%
print (vectorizer.fit_transform(corpus).toarray())
print (vectorizer.get_feature_names())

# %%
from sklearn.feature_extraction.text import HashingVectorizer 
vectorizer2=HashingVectorizer(n_features = 6,norm = None)
print (vectorizer2.fit_transform(corpus))

# %%
