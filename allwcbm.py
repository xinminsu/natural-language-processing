# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

用gensim学习word2vec https://www.cnblogs.com/pinard/p/7278324.html
"""

# %%
# -*- coding: utf-8 -*-

import jieba
import jieba.analyse
import logging
import os
from gensim.models import word2vec



model = word2vec.Word2Vec.load("allwancibiao_guoji.model")
# %%
req_count = 5
for key in model.wv.similar_by_word('数码', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------1-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('显示器', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------2-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('风衣', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------3-----------")
# %%
req_count = 5
for key in model.wv.similar_by_word('补肾', topn =100):
    if len(key[0])==3:
        req_count -= 1
        print(key[0], key[1])
        if req_count == 0:
            break;
print("---------4-----------")
# %%
print(model.wv.similarity('蓝牙', '耳机'))
print("---------5-----------")
print(model.wv.similarity('女士内衣', '男士内衣'))
print("---------6-----------")
# %%
print(model.wv.doesnt_match(u"女装 女士 精品 唐装 眼镜".split()))
print("---------7-----------")
# %%
 