# %%

# %%
# -*- coding: utf-8 -*-

from gensim.models import word2vec
import jieba
import jieba.analyse
import logging
import os

import sys, getopt

def genModel(inputFile):

    segmentName = os.path.splitext(inputFile)[0] + "_segment.txt"

    with open(inputFile,'r', encoding = 'utf-8') as f:
        document = f.read()

        document_cut = jieba.cut(document)

        result = ' '.join(document_cut)

        with open(segmentName, 'w', encoding = 'utf-8') as f2:
            f2.write(result)
    f.close()
    f2.close()



    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    sentences = word2vec.LineSentence(segmentName)

    model = word2vec.Word2Vec(sentences, hs=1,min_count=1,window=3,size=100)

    model.save(os.path.splitext(inputFile)[0] + ".model")

def main(argv):
   inputFile = ''

   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('model.py -i <inputFile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print( 'model.py -i <inputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputFile = arg

   if inputFile == '':
       print('model.py -i <inputFile>')
       sys.exit(2)

   genModel(inputFile)

   print('finished')

if __name__ == "__main__":
   main(sys.argv[1:])
 