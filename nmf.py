# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

文本主题模型之非负矩阵分解(NMF) https://www.cnblogs.com/pinard/p/6812011.html
"""

# %%
import numpy as np
X = np.array([[1,1,5,2,3], [0,6,2,1,1], [3, 4,0,3,1], [4, 1,5,6,3]])
from sklearn.decomposition import NMF
model = NMF(n_components=2, alpha=0.01)

# %%
W = model.fit_transform(X)
H = model.components_
print(W)
print(H)

# %%
