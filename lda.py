# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

用scikit-learn学习LDA主题模型 https://www.cnblogs.com/pinard/p/6908150.html
"""

# %%
# -*- coding: utf-8 -*-

import jieba

stpwrdpath = "stop_words.txt"
stpwrd_dic = open(stpwrdpath, 'rb')
stpwrd_content = stpwrd_dic.read()
#将停用词表转换为list
stpwrdlst = stpwrd_content.splitlines()
stpwrd_dic.close()

# %%
with open('./nlp_test1.txt') as f3:
    res1 = f3.read()
print(res1)

# %%
with open('./nlp_test3.txt') as f4:
    res2 = f4.read()
print(res2)

# %%
with open('./nlp_test5.txt') as f5:
    res3 = f5.read()
print(res3)

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
corpus = [res1,res2,res3]
cntVector = CountVectorizer(stop_words=stpwrdlst)
cntTf = cntVector.fit_transform(corpus)
print(cntTf)

# %%
lda = LatentDirichletAllocation(n_topics=2,
                                learning_offset=50.,
                                random_state=0)
docres = lda.fit_transform(cntTf)

# %%
print(lda.components_)

# %%
print(docres)

# %%


# %%
