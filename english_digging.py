# %%
"""
Copyright (C) 2016 - 2019 Pinard Liu(liujianping-ok@163.com)

https://www.cnblogs.com/pinard

Permission given to modify the code as long as you keep this declaration at the top

英文文本挖掘预处理流程总结 https://www.cnblogs.com/pinard/p/6756534.html
"""

# %%
import nltk
nltk.download()

# %%
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()
print(wnl.lemmatize('imaging'))  

# %%
from nltk.stem import SnowballStemmer
print(" ".join(SnowballStemmer.languages)) # See which languages are suppo

# %%
stemmer = SnowballStemmer("english") # Choose a language
stemmer.stem("countries") # Stem a word

# %%
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()
print(wnl.lemmatize('countries'))  

# %%
from enchant.checker import SpellChecker
chkr = SpellChecker("en_US")
chkr.set_text("Many peope likee to watch In the Name of People.")
for err in chkr:
    print "ERROR:", err.word

# %%


# %%
